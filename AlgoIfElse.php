<?php

/** 
 * Exercice Bob
 *  Bob est un robot avec une conversation très limitée. Ecrivez sa fonction.
 *  Bob dit 'Ouais.' lorsque vous lui posez une question. Ex : bob("Tu vas bien ?")--> 'Ouais'
 *  Il fait 'Humm' si vous ne lui dites rien. Ex: bob("") --> 'Humm'
 *  Dans tous les autre cas il réponds 'Ok'

    */
function bob($message){
    return 'Ok';
}