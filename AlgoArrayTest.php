<?php
require "AlgoArray.php";
use PHPUnit\Framework\TestCase;
class AlgoArrayTest extends TestCase {
    /**
     * Ecrire un algorithme qui déclare et remplisse un tableau de 7 valeurs numériques
     * en les mettant toutes à zéro.
     */
    public function testArrayFullOfZeroes7() {
        $this->assertEquals([0,0,0,0,0,0,0], arrayFullOfZeroes());
    }

    /**
     * Faites en sorte que la fonction prenne en compte un paramètre. 
     * Le paramètre spécifie le nombre de valeurs contenues dans le tableau. 
     */
    public function testArrayFullOfZeroes0() {
        $this->markTestSkipped();
        $this->assertEquals([], arrayFullOfZeroes(0));
    }
    
    /**
     * Exo 5.5
     * Ecrivez un algorithme constituant un tableau, à partir de deux tableaux de
     * même longueur préalablement saisis. Le nouveau tableau sera la somme des
     * éléments des deux tableaux de départ.
     */
    public function testSumOfArrays() {
        $this->markTestSkipped();
        $this->assertEquals([11,14,12,11,2,8,11,10], sumOfArrays([4,8,7,9,1,5,4,6], [7,6,5,2,1,3,7,4]));
    }

    /**
     * Exo 5.5 - bonus
     * Si les tableaux fournis ont une taille différente, 
     * la fonction doit uniquement réaliser la somme des valeurs possibles sur la longueur la plus petite
     */
    public function testSumOfArrays2() {
        $this->markTestSkipped();
        $this->assertEquals([11,14,12,11,2,8], sumOfArrays([4,8,7,9,1,5], [7,6,5,2,1,3,7,4]));
        $this->assertEquals([11,14,12,11,2,8], sumOfArrays([4,8,7,9,1,5,4,6], [7,6,5,2,1,3]));
    }

    /**
     * Exo 5.6
     *  écrivez un algorithme
     * qui réalise le calcul suivant: il faut multiplier chaque élément du tableau 1 par
     * chaque élément du tableau 2, et additionner le tout.
     */
    public function testMultiplicationOfArrays() {
        $this->markTestSkipped();
        $this->assertEquals(3*4 + 3*8 + 3*7 + 3*12 + 6*4 + 6*8 + 6*7 + 6*12, sumOfMultiplicationOfArrays([4,8,7,12], [3,6]));
    }

    /**
     * Exo 5.7
     * La fonction  renvoie la plus grande valeur d'un tableau 
     * en précisant quelle position elle occupe dans le tableau
     */
    public function testMaxAndPositionInArray() {
        $this->markTestSkipped();
        $this->assertEquals([2,20], maxValueAndPosition([12, 9, 20, 18]));
    }
    /**
     * Exo 5.7 - bonus
     */
    public function testMaxAndPositionInNegativeArray() {
        $this->markTestSkipped();
        $this->assertEquals([3,-1], maxValueAndPosition([-12, -9, -20, -1, -8]));
    }
}