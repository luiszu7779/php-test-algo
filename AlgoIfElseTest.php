<?php

require "AlgoIfElse.php";
use PHPUnit\Framework\TestCase;
class AlgoIfElseTest extends TestCase
{
    /** 
     * Exercice Bob
     *  Bob est un robot avec une conversation très limitée. Ecrivez sa fonction.
     *  Bob dit 'Ouais.' lorsque vous lui posez une question. Ex : bob("Tu vas bien ?")--> 'Ouais'
     *  Il fait 'Humm' si vous ne lui dites rien. Ex: bob("") --> 'Humm'
     *  Dans tous les autre cas il réponds 'Ok'

     */
    public function testBobSaysHumm()
    {
        $this->assertEquals('Humm', bob(''));
    }
    public function testBobSaysOk() {
        $this->assertEquals('Ok', bob('Je mange des spaghettis.'));
    }
    public function testBobSaysYes() {
        $this->assertEquals('Ouais.', bob('Il fait beau aujourd\'hui ?'));
    }

}