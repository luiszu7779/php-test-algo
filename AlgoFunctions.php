<?php

/**
 * Cesar
 * 
 * Exercice 8.5
 * Un des plus anciens systèmes de cryptographie (aisément déchiffrable) se nomme
 * CESAR et consiste à décaler les lettres d’un message pour le rendre illisible. On
 * peut décaller les lettes du pas que l’on veut. Mais prenons le cas où le pas est
 * de 1. Ainsi, les A deviennent des B, les B des C, etc. Ecrivez un algorithme qui
 * demande une phrase à l’utilisateur et qui la code selon ce principe. Attention,
 * le Z doit devenir un A.
 * Comme dans le cas précédent, le codage doit s’effectuer au niveau de la variable
 * stockant la phrase, et pas seulement à l’écran.
 * 1
 * Exercice 8.6
 * Une amélioration (relative) du principe précédent consiste à opérer avec un
 * décalage non de 1, mais d’un nombre quelconque de lettres. Ainsi, par exemple,
 * si l’on choisit un décalage de 12, les A deviennent des M, les B des N, etc.
 * Réalisez un algorithme sur le même principe que le précédent, mais qui demande
 * en plus quel est le décalage à utiliser.
 */
function cesar(string $phrase, int $step = 1) : string
{
    //
    // YOUR CODE GOES HERE
    //
}

/**
 * Exercice 8.7
 * Une technique ultérieure de cryptographie consista à opérer non avec un dé-
 * calage systématique, mais par une substitution aléatoire. Pour cela, on utilise
 * un alphabet-clé, dans lequel les lettres se succèdent de manière désordonnée, par
 * exemple :HYLUJPVREAKBNDOFSQZCWMGITX
 * C’est cette clé qui va servir ensuite à coder le message. Selon notre exemple,
 * les A deviendront des H, les B des Y, les C des L, etc. Ecrire un algorithme
 * qui effectue ce cryptage (l’alphabet-clé sera saisi par l’utilisateur, et on suppose
 * qu’il effectue une saisie correcte).
 */
function arrayMapping(string $message, string $mapping) : string {

}

/**
 * Bonus : Algo Roman to Arabic
 */
function romanToArabic(string $roman) : int {

}